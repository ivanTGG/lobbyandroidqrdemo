package com.tgg.lobby;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.scottyab.aescrypt.AESCrypt;

import java.io.IOException;
import java.security.GeneralSecurityException;

import android.graphics.BitmapFactory;
import android.util.SparseArray;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

public class MainActivity extends AppCompatActivity  {

    static String password = "L033YQR";
    String scannedStr = "";

    /**
     * Id to identify a camera permission request.
     */
    private static final int REQUEST_CAMERA = 0;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 0;

    private View mLayout;

    CallbackManager callbackManager;
    ShareDialog shareDialog;

    CameraSource cameraSource;
    SurfaceView cameraView;

    View sharingView;

//    @Override
//    protected void onResume() {
//        super.onResume();
////                cameraSource.start(cameraView.getHolder());
//
//        Log.i("TAG","Resume");
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mLayout = findViewById(R.id.activity_main);

        Log.i("Tag","Lobby Start");

        fbShareCheck();
        checkPlayServices();

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

//        String message = "LOBBYQR|Steve|100000|777";
//        try {
//            String encryptedMsg = AESCrypt.encrypt(password, message);
//            Log.i("encrypt",encryptedMsg);
//        }catch (GeneralSecurityException e){
//            //handle error
//            Log.i("encrypt","error");
//        }
//
//        String encryptedMsg = "YpTS/CLKd48tjvDykscAdqrhsGJgaXPxGyLRDmvV5Q4=";
//
//        try {
//            String messageAfterDecrypt = AESCrypt.decrypt(password, encryptedMsg);
//            Log.i("decrypt",messageAfterDecrypt);
//        }catch (GeneralSecurityException e){
//            //handle error - could be due to incorrect password or tampered encryptedMsg
//            Log.i("decrypt","error");
//        }

        cameraView = (SurfaceView)findViewById(R.id.camera_view);
        final TextView barcodeInfo = (TextView)findViewById(R.id.code_info);

        final TextView textName = (TextView)findViewById(R.id.textName);
        final TextView textAmount = (TextView)findViewById(R.id.textAmount);
        final ImageView gameImage = (ImageView)findViewById(R.id.gameImage);
        sharingView = (View)findViewById(R.id.sharingView);

        BarcodeDetector barcodeDetector =
                new BarcodeDetector.Builder(this)
                        .setBarcodeFormats(Barcode.QR_CODE)
                        .build();
        cameraSource = new CameraSource
                .Builder(this, barcodeDetector)
                .setRequestedPreviewSize(640, 480)
                .build();

        cameraView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                tryLaunchQR();
//                cameraSource.start(cameraView.getHolder());
            }
            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            }
            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                cameraSource.stop();
            }
        });
        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {
            }
            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> barcodes = detections.getDetectedItems();
                if (barcodes.size() != 0) {
                    barcodeInfo.post(new Runnable() {    // Use the post method of the TextView
                        public void run() {

//                            barcodeInfo.setText(barcodes.valueAt(0).displayValue);
                            String encryptedMsg = barcodes.valueAt(0).displayValue;
                            Log.i("scan",encryptedMsg);

                            if (!(scannedStr.equals(encryptedMsg))) {
                                scannedStr = encryptedMsg;

                                try {
                                    String messageAfterDecrypt = AESCrypt.decrypt(password, encryptedMsg);
                                    Log.i("decrypt",messageAfterDecrypt);

                                    if(messageAfterDecrypt.startsWith("LOBBYQR|")) {

                                        cameraSource.stop();

                                        String[] splited = messageAfterDecrypt.split("[|]");
                                        String name = splited[1];
                                        String amount = splited[2];
                                        String game = splited[3];
//                                        Log.i("tag",name+","+amount+","+game);

                                        String nameDisplayed = name+" just won";
                                        textName.setText(nameDisplayed);
                                        String amountDisplayed = "$"+amount;
                                        textAmount.setText(amountDisplayed);

                                        int gameImageRes = R.drawable.game777;
                                        switch (game) {
                                            case "777":
                                                gameImageRes = R.drawable.game777;
                                                break;
                                            case "3kingdoms":
                                                gameImageRes = R.drawable.threekingdoms;
                                                break;
                                            case "happyfarm":
                                                gameImageRes = R.drawable.happyfarm;
                                                break;
                                        }
                                        Bitmap gameImageBitmap = BitmapFactory.decodeResource(getResources(), gameImageRes);
                                        gameImage.setImageBitmap(gameImageBitmap);

                                        shareFB(sharingView);
                                    }

                                }catch (GeneralSecurityException e){
                                    //handle error - could be due to incorrect password or tampered encryptedMsg
                                    Log.i("decrypt","error");
                                }
                            }
                        }
                    });
                }
            }
        });
    }

    public void tryLaunchQR() {
        // Check if the Camera permission is already available.
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            // Camera permission has not been granted.
            requestCameraPermission();
        } else {
            // Camera permissions is already available, show the camera preview.
            try {
                cameraSource.start(cameraView.getHolder());
            } catch (IOException ie) {
                Log.e("CAMERA SOURCE", ie.getMessage());
            }
        }
    }

    /**
     * Requests the Camera permission.
     * If the permission has been denied previously, a SnackBar will prompt the user to grant the
     * permission, otherwise it is requested directly.
     */
    private void requestCameraPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.CAMERA)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.
            Snackbar.make(mLayout, R.string.permission_camera_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ActivityCompat.requestPermissions(MainActivity.this,
                                    new String[]{Manifest.permission.CAMERA},
                                    REQUEST_CAMERA);
                        }
                    })
                    .show();
        } else {
            // Camera permission has not been granted yet. Request it directly.
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},
                    REQUEST_CAMERA);
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        if (requestCode == REQUEST_CAMERA) {

            // Received permission result for camera permission.est.");
            // Check if the only required permission has been granted
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Camera permission has been granted, preview can be displayed
                Snackbar.make(mLayout, R.string.permision_available_camera,
                        Snackbar.LENGTH_SHORT).show();
                tryLaunchQR();
            } else {
                Snackbar.make(mLayout, R.string.no_camera_permission,
                        Snackbar.LENGTH_SHORT).show();

            }
        }
    }

    void shareFB(View v) {
        Bitmap image = getViewBitmap(v);
        SharePhoto photo = new SharePhoto.Builder()
                .setBitmap(image)
                .build();
        SharePhotoContent content = new SharePhotoContent.Builder()
                .addPhoto(photo)
                .build();

        shareDialog = new ShareDialog(this);
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {

            @Override
            public void onSuccess(Sharer.Result result) {
                Log.i("TAG","success");
                tryLaunchQR();
;           }
            @Override
            public void onCancel() {
                Log.i("TAG","cancel");
                tryLaunchQR();
            }
            @Override
            public void onError(FacebookException error) {
                Log.i("TAG","error");
                tryLaunchQR();
            }
        });
        Log.i("TAG","1!");

        if (ShareDialog.canShow(SharePhotoContent.class)) {

            Log.i("TAG","2!");
//            ShareLinkContent linkContent = new ShareLinkContent.Builder()
//                    .setContentTitle("Hello Facebook")
//                    .setContentDescription(
//                            "The 'Hello Facebook' sample  showcases simple Facebook integration")
//                    .setContentUrl(Uri.parse("http://developers.facebook.com/android"))
//                    .build();
            shareDialog.show(content);
        }
        else {
            FBFailDialog myDialog = new FBFailDialog();
            myDialog.show(getFragmentManager(), "123");
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    void fbShareCheck() {
        Log.i("FB check","start");
        if (ShareDialog.canShow(SharePhotoContent.class)) {
            // do nothing
            Log.i("FB check","pass");
        }
        else {
            Log.i("FB check","fail");
            FBFailDialog myDialog = new FBFailDialog();
            myDialog.show(getFragmentManager(), "123");
        }
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            Log.i("PS check","fail 1");
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
                Log.i("PS check","fail 1.1");
            } else {
                Log.i("TAG", "This device is not supported.");
                PlayServiceFailDialog myDialog = new PlayServiceFailDialog();
                myDialog.show(getFragmentManager(), "123");
                Log.i("PS check","fail 1.2");
            }
            return false;
        }
        Log.i("PS check","pass");
        return true;
    }
    private Bitmap getViewBitmap(View v) {
        v.clearFocus();
        v.setPressed(false);

        boolean willNotCache = v.willNotCacheDrawing();
        v.setWillNotCacheDrawing(false);

        // Reset the drawing cache background color to fully transparent
        // for the duration of this operation
        int color = v.getDrawingCacheBackgroundColor();
        v.setDrawingCacheBackgroundColor(0);

        if (color != 0) {
            v.destroyDrawingCache();
        }
        v.buildDrawingCache();
        Bitmap cacheBitmap = v.getDrawingCache();
        if (cacheBitmap == null) {
            Log.e("Error", "failed getViewBitmap(" + v + ")", new RuntimeException());
            return null;
        }

        Bitmap bitmap = Bitmap.createBitmap(cacheBitmap);

        // Restore the view
        v.destroyDrawingCache();
        v.setWillNotCacheDrawing(willNotCache);
        v.setDrawingCacheBackgroundColor(color);

        return bitmap;
    }
}
