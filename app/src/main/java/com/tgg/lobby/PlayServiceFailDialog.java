package com.tgg.lobby;

/**
 * Created by ivanwan on 4/3/2017.
 */

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

public class PlayServiceFailDialog extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        // Use the Builder class because this dialog has a simple UI
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage("Google Play Service is required for using QR scanning function. Please go to Google Play, download \"Google Play Service\" and try again.")
                // An OK button that does nothing
                .setPositiveButton("Close app", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        getActivity().finish();
                        System.exit(0);
                    }
                });

        // Create the object and return it
        return builder.create();
    }
}