package com.tgg.lobby;

/**
 * Created by ivanwan on 3/3/2017.
 */

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;


public class FBFailDialog extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        // Use the Builder class because this dialog has a simple UI
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage("Seems there is some problem using the existed Facebook app process to share a picture. Please re-open Facebook app and try again.")
                // An OK button that does nothing
                .setPositiveButton("Close app", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        getActivity().finish();
                        System.exit(0);
                    }
                });

        // Create the object and return it
        return builder.create();
    }
}
